#include <arch/mempool.h>
#include <arch/vmscan.h>
#include <xbook/debug.h>

extern mem_range_t mem_ranges[MEM_RANGE_NR];

#define lru_to_page(_head) (list_owner((_head)->prev, mem_node_t, list))

enum page_references {
	PAGEREF_RECLAIM,		/* 尝试清理 */
	PAGEREF_RECLAIM_CLEAN,	/* 尝试清理 */
	PAGEREF_KEEP,			/* 保持在不活跃链表 */
	PAGEREF_ACTIVATE,		/* 迁移到活跃链表 */
};

static void move_active_pages_to_lru(mem_range_t *mem_range,
				     list_t *list,
				     enum lru_list lru)
{
	mem_node_t *mem_node;
	while (!list_empty(list)) {
		mem_node = lru_to_page(list);
		SetPageLRU(mem_node);
		list_move(&mem_node->list, &mem_range->lruvec.lists[lru]);
	}
}

/**
 * 在一次扫描间隔中, mem_node 被访问的次数(mem_node 包含多个物理页)
 * 首先根据 mem_node_t 找到对应的pte。
 */
int mem_node_referenced (mem_node_t *mem_node) {
    // mem_node->vmm
    return 0;
}
/**
 * 对于 inactive list 中的 mem_node 进行判断。
 */
static enum page_references mem_node_check_references(mem_node_t *mem_node) {
	int referenced_ptes, referenced_page;
    referenced_ptes = mem_node_referenced(mem_node);
	referenced_page = TestClearPageReferenced(mem_node);
	/* 如若近期被访问过 */
	if (referenced_ptes) {
        return PAGEREF_ACTIVATE;
	}
	/* 如若近期没被访问过且有引用位 */
	if (referenced_page)
		return PAGEREF_RECLAIM_CLEAN;
	return PAGEREF_RECLAIM;
}

/**
 * 活跃链表转换转换
 * 1. 如若近期未访问,将其放入不活跃链表中
 * 2. 如若近期访问了,无动作。
 */
static void shrink_active_list (mem_range_t *mem_range) {
    mem_node_t *mem_node;
	LIST_HEAD(l_active);	/* 活跃链表 */
	LIST_HEAD(l_inactive);	/* 不活跃链表 */
	spin_lock_irq(&mem_range->lru_lock);
    // infoprint("shrink_active_list ================================================================================\n");
    list_t* lru_list = &mem_range->lruvec.lists[LRU_ACTIVE_ANON];
    while (!list_empty(lru_list)) {
        mem_node = lru_to_page(lru_list);
        list_del(&mem_node->list);
        if (mem_node_referenced(mem_node)){
            list_add(&mem_node->list, &l_active);
            continue;
        }
        ClearPageActive(mem_node);
        list_add(&mem_node->list, &l_inactive);
    }
    /* 将链表中的页移回lru去。 */
	move_active_pages_to_lru(mem_range, &l_active, LRU_ACTIVE_ANON);
    move_active_pages_to_lru(mem_range, &l_inactive, LRU_INACTIVE_ANON);
    spin_unlock_irq(&mem_range->lru_lock);
}

/**
 * 不活跃链表转换转换
 * 1.如若是有引用则
 */
static unsigned long shrink_inactive_list (mem_range_t *mem_range) {
    mem_node_t *mem_node;
	LIST_HEAD(l_active);	/* 活跃链表 */
	LIST_HEAD(l_inactive);	/* 不活跃链表 */
    LIST_HEAD(l_swap);	    /* 交换区 */
	spin_lock_irq(&mem_range->lru_lock);
    list_t* lru_list = &mem_range->lruvec.lists[LRU_INACTIVE_ANON];
    // infoprint("shrink_inactive_list ================================================================================\n");
    while (!list_empty(lru_list)) {
        mem_node = lru_to_page(lru_list);
        list_del(&mem_node->list);
        switch (mem_node_check_references(mem_node)){
        case PAGEREF_ACTIVATE:
            list_add(&mem_node->list, &l_active);
            break;
        case PAGEREF_KEEP:
            list_add(&mem_node->list, &l_inactive);
            break;
        case PAGEREF_RECLAIM:
	    case PAGEREF_RECLAIM_CLEAN:
            list_add(&mem_node->list, &l_swap);
            break;
        default:
            // 错误处理
            return -1;
        }
    }
    /* 将链表中的页移回lru去。 */
	move_active_pages_to_lru(mem_range, &l_active, LRU_ACTIVE_ANON);
    move_active_pages_to_lru(mem_range, &l_inactive, LRU_INACTIVE_ANON);
    spin_unlock_irq(&mem_range->lru_lock);
    return 0;
}

static unsigned long shrink_list (mem_range_t* mem_range, enum lru_list lru) {
    if (is_active_lru(lru)) {
        shrink_active_list(mem_range);
        return 0;
    }
    return shrink_inactive_list(mem_range);
}

static void shrink_mem_range (mem_range_t* mem_range) {
    int i;
    for (i = LRU_INACTIVE_ANON; i < NR_LRU_LISTS; i){
            shrink_list(mem_range, LRU_INACTIVE_ANON);
    }
    return;
}

void shrink_mem_ranges () {
    int i;
    for (i = 0; i < MEM_RANGE_NR; i) {
        shrink_mem_range(&mem_ranges[i]);
    }
}