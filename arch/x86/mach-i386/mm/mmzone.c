#include <arch/mmzone.h>
#include <arch/page.h>
#include <xbook/list.h>
#include <arch/phymem.h>


void free_area_init(struct free_area *free_area_p){
    int i;
    for(i=0; i < MIGRATE_TYPES; i++){
        list_init(&free_area_p->free_list[i]);
    }
    //TODO  这里分配自由页个数
    free_area_p->nr_free=0;//初始化为0
}
void mem_page_init(struct page *mem_page, unsigned long flags_p){
    //page结构体初始化
    mem_page->flags = flags_p;
    mem_page->count.value = 0; //页的使用记录初始记为0
    list_init(&mem_page->list);
    //这里的struct address_space *mapping; //本页所归属的节点不知道怎么写
    mem_page->index = 0; //初始没有偏移量
    list_init(&mem_page->lru);
}
//type是zone的类型
void mem_zone_init(struct zone *mem_zone, unsigned int type, size_t zone_size)  
{   
    mem_zone->free_pages = zone_size / PAGE_SIZE;
    mem_zone->pages_min = (mem_zone->free_pages / 10);//这里是判断zone空闲页的指标，总数的十分之一即为最小值
    mem_zone->pages_low = (mem_zone->free_pages / 3);//三分之一为低内存标志
    mem_zone->pages_high = ((mem_zone->free_pages / 4) * 3);//大于四分之三则为内存充裕
    list_init(&mem_zone->active_list);//初始化活动页列表
    list_init(&mem_zone->inactive_list);//初始化静态页列表
    int i;
    for(i=0; i < MAX_ORDER; i++){
        free_area_init(&mem_zone->free_area[i]);
    }
    //TODO
    //struct pglist_data *zone_pgdat; //该区域和父节点的映射
    //struct page *zone_mem_map;      //区域中页的映射集合
    /* zone_start_pfn == zone_start_paddr >> PAGE_SHIFT */
    //unsigned long zone_start_pfn; //内存域第一个页帧的索引
    //和父节点的映射，这里应该怎么写？
    //page数组也不知道怎么实现~~~
    //分配了页数组之后才能给出zone_start_pfn
    switch(type){
        case 0 :   mem_zone->name = "ZONE_DMA";break;
        case 1 : mem_zone->name = "ZONE_NORMAL";break;
        case 2 : mem_zone->name = "ZONE_USER";break;
        default : return;
    }
    //剩下两个也不知道怎么写。。。 
    //TODO
    //unsigned long spanned_pages; //指定内存域中页的总数，但并非所有的都可用，因为有空洞
    //unsigned long present_pages; //指定了内存域中实际上可用的页数目
}

//节点初始化函数，idx是节点序号，start和end是节点的起始地址和结束地址
void mem_pg_data_init(unsigned int idx, size_t total_phy)
{   
    if (idx > MAX_NUMNODES)  //本系统如今只支持单节点，所以idx只能为1，大于1直接返回
        return;
    //将bookOS的物理区域划分放进这个函数中,与bookOS的物理内存划分区分开
    unsigned int normal_size;
    unsigned int user_size;
    unsigned int unused_size;
    
    unused_size = KERN_BLACKHOLE_MEM_SIZE + NORMAL_MEM_ADDR;
    normal_size = (total_phy - unused_size) / 2; 
    user_size = total_phy - unused_size - normal_size;
    
    if (normal_size > 1*GB) {
        unsigned int more_size = normal_size - 1*GB;
        user_size += more_size;
        normal_size -= more_size;
    }
    //以上和bookOS的处理一样
    unsigned int i=0;//用来记录node_id，全部节点数的初始化只需要一次
    while(idx){
        pg_data_t *mem_node = &mem_node[idx];  //初始化节点数组（这里数组只有一个元素，但是可以为以后留下升级空间）
        mem_zone_init(&mem_node->node_zones[ZONE_DMA], ZONE_DMA, DMA_MEM_SIZE);
        mem_zone_init(&mem_node->node_zones[ZONE_NORMAL], ZONE_NORMAL, normal_size);
        mem_zone_init(&mem_node->node_zones[ZONE_USER], ZONE_USER, user_size);
        mem_node->nr_zones = 3; //每个节  
        mem_node->node_start_pfn = 0;//这里不知道该怎么设置，第一个页帧的逻辑号，我设置了0
        mem_node->node_id = i++;//node_id从0开始
        --idx;//迭代节点
    }
}
 
    





