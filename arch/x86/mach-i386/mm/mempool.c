#include <arch/mempool.h>
#include <arch/page.h>
#include <arch/bootmem.h>
#include <xbook/debug.h>
#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <arch/vmscan.h>

// #define MEMPOOL_DEBUG

mem_range_t mem_ranges[MEM_RANGE_NR];

void mem_node_init(mem_node_t *node, int ref, uint32_t size)
{
    node->reference = ref;                              //这里有一个ref，通过读代码，由于在初始化时为每一个page都分配了node结构体，那么这里的ref应该是占用与否
    node->count = size;                                 //count应该是该节点中的page数目
    node->cache = NULL;
    node->group = NULL;
    node->section = NULL;
    list_init(&node->list);
}

void mem_section_init(mem_section_t *mem_section, size_t section_size)   //在section初始化时，因为没有使用任何node，故只需要将该section的node的尺寸定下来，并初始化free链表
{   
    mem_section->node_count = 0;
    mem_section->section_size = section_size;
    list_init(&mem_section->free_list_head);
}

void mem_section_setup(mem_section_t *mem_section, mem_node_t *node_base, size_t section_count)  //这里的section_count是有几个节数，例：最大阶的section中的节点能容纳2048个page，那么section_count则表示该阶有几个容纳2048page的节点
{    
    mem_section->node_count = section_count;   //section_count即节点数
    int i;
    for (i = 0; i < section_count; i++) {
        mem_node_t *next = node_base + i * mem_section->section_size;  //这里面是按section_count存储
        mem_node_init(next, 0, 0);                                     //将节点初始化，即ref为0，页数为0
        list_add_tail(&next->list, &mem_section->free_list_head);      //将这个节点加入到当前section的可用链表中
        MEM_NODE_MARK_SECTION(next, mem_section);
    }
}

mem_range_t *mem_range_get_by_mem_node(mem_node_t *node)
{
    if (!node)
        return NULL;
    int i;
    mem_range_t *mem_range;
    for (i = 0; i < MEM_RANGE_NR; i++) {
        mem_range = &mem_ranges[i];
        if (mem_range->node_table <= node && node < mem_range->node_table + mem_range->pages) {
            return mem_range;
        } 
    }
    emeprint("mem_range_get_by_mem_node: node %x\n", node);
    panic("mem node not in range!");
    return NULL;
}

mem_range_t *mem_range_get_by_phy_addr(unsigned int addr)
{
    if (!addr)
        return NULL;
    int i;
    mem_range_t *mem_range;
    for (i = 0; i < MEM_RANGE_NR; i++) {
        mem_range = &mem_ranges[i];
        if (mem_range->start <= addr && addr < mem_range->end) {
            return mem_range;
        } 
    }
    emeprint("mem_range_get_by_phy_addr: addr %x\n", addr);
    panic("addr not in range!");
    return NULL;
}

static void lruvec_init (lruvec_t* lruvec){
    int i;
    for (i = LRU_INACTIVE_ANON; i < NR_LRU_LISTS; i++){
            list_init(&lruvec->lists[i]);
    }
    return;
}

void mem_range_init(unsigned int idx, unsigned int start, size_t len)  //这里idx是指初始化哪个zone
{
    if (idx >= MEM_RANGE_NR)
        return;
    mem_range_t *mem_range = &mem_ranges[idx];  //静态数组，保留有三个zone的信息
    spinlock_init(&mem_range->lru_lock);
    lruvec_init(&mem_range->lruvec);
    mem_range->start = start;
    mem_range->end = start + len;
    mem_range->pages = len / PAGE_SIZE;          //该zone的页数
    mem_range->node_table = boot_mem_alloc(mem_range->pages * sizeof(mem_node_t));  //页数*node结构体的大小，即，给每一个page都留了一个node，等待buddy分割后使用
    if (mem_range->node_table == NULL) {
        panic("mem range %d: start=%x len=%x node table alloc null!\n",
            idx, start, len);  
    }
    int i;
    for (i = 0; i < MEM_SECTION_MAX_NR; i++) {      //section相当于buddy系统，0~12阶，比buddy多了一阶
        mem_section_init(&mem_range->sections[i], powi(2, i));
    }                                               //这个循环结束后，该range的12个阶就已经定下了（即每个阶容纳的node中的page数定下来了），空闲链表也已经初始化完成
    
    int section_off = MEM_SECTION_MAX_NR - 1;     
    size_t section_size = powi(2, section_off);     //section_off是当前的阶数，2^section_off就是当前阶的node所能容纳的内存页数，section_size是这个section的node能够容纳多少页
    mem_section_t *mem_section;
    size_t big_sections;
    size_t small_sections;
    
    mem_node_t *node_base = mem_range->node_table;    //node_tabel存放的是node结构体的地址，前面申请了当前range的pages个结构体。
    ssize_t pages = mem_range->pages;

    while (pages > 0) {
        assert(0 <= section_off && section_off < MEM_SECTION_MAX_NR);

        big_sections = pages / section_size;                 //首先从最大阶开始分配，这里表示有多少个最大阶
        small_sections = pages % section_size;               //大阶分配完，剩下的给予最小的0阶
        mem_section = &mem_range->sections[section_off];     //这里现在是最大阶，即11阶
        
        if (big_sections > 0) {
            mem_section_setup(mem_section, node_base, big_sections);  //这将最大阶全部分配完，看到这里的node_base了么？将最大阶的2048个页当作一个节点，放了进去
            pages -= big_sections * section_size;                     //剩余的页数
            node_base += big_sections * section_size;                 //boot_mem_alloc分配到了哪里，记录下当前的boot_mem_alloc.cur
        } else if (small_sections > 0) {
            /* 把剩下的所有小的节都放到大小为1的节中。 */
            mem_section = &mem_range->sections[0];                    //分配到最小阶中
            mem_section_setup(mem_section, node_base, small_sections);  //这时section[0]拥有了small_sections个node，每个node有一个page
            break;
        }
        
        section_size >>= 1;    //阶数-1，即二的指数少一个    ——这里的执行流只会是在最小节为0时进入，最小节大于0时会触发break
        --section_off;         //开始分配低一阶
    }
}

mem_node_t *phy_addr_to_mem_node(unsigned int addr)
{ 
    if (!addr)
        return NULL;
    mem_range_t *mem_range = mem_range_get_by_phy_addr(addr);
    if (!mem_range)
        return NULL;
    unsigned int local_addr = addr - mem_range->start;
    unsigned int index = local_addr >> PAGE_SHIFT;
    mem_node_t *node = mem_range->node_table + index;
    return node;
}

unsigned int mem_node_to_phy_addr(mem_node_t *node)
{ 
    if (!node)
        return 0;
    mem_range_t *mem_range = mem_range_get_by_mem_node(node);
    if (!mem_range)
        return 0;
    unsigned int index =  node - mem_range->node_table;
    unsigned int local_addr = index << PAGE_SHIFT;
    return local_addr + mem_range->start; 
}

int mem_range_split_section(mem_range_t *mem_range, mem_section_t *mem_section)  //buddy系统中的分割机制,这里传入的参数，section已经定好是哪一阶了，在该阶中，节点的页数是固定的
{
    if (!mem_range || !mem_section)
        return 0;
    /* 已经有节有空闲的节点，成功返回！ */
    if (!list_empty(&mem_section->free_list_head))              
        return 0;

    mem_section_t *tmp_section = mem_section + 1;     //buddy中，当前阶不够用，当然要向上一阶要一个node
    mem_section_t *top_section = &mem_range->sections[MEM_SECTION_MAX_NR - 1];  //最大阶的section
    while (tmp_section <= top_section) {    //当tmp还没有到最高阶时，需要判断当前section的自由链表是否有值，若有，则找到了空闲的node，若没有则再到上一阶找
        if (!list_empty(&tmp_section->free_list_head))
            break;
        tmp_section++;
    }
    
    if (tmp_section > top_section) {         //如果tmp比最大阶要高了，说明当前系统里面没有足够的内存分配了，需要进行buddy的整理
        // TODO: 收缩内存
        mem_range_merge_section(mem_range);
        //keprint(PRINT_ERR "mempool: no free section left!\n");
        return 0;
    }
    //执行流走到这里，说明当前的tmp_section有空闲node
    mem_node_t *node = list_first_owner(&tmp_section->free_list_head, mem_node_t, list);  //找到这个空闲链表的第一个成员节点
    list_del(&node->list);                    //将这个节点从当前section的free_list_head链表中删除
    MEM_SECTION_DES_COUNT(tmp_section);       //当前tmp_section的node数-1

    mem_node_init(node, 1, tmp_section->section_size / 2);    //刚刚拿到的这个node，一分为二，初始化为一个新的node
    
    mem_node_t *node_half = node + node->count;               //剩下的那一半，也初始化一个新node，这样就有了两个比tmp_section低一阶的node
    mem_node_init(node_half, 1, node->count);

    --tmp_section; // 下降一个节高度
    list_add(&node->list, &tmp_section->free_list_head);          //将这两个node加入当前阶
    list_add(&node_half->list, &tmp_section->free_list_head);
    MEM_NODE_MARK_SECTION(node, tmp_section);                     //标记这两个node的section归属
    MEM_NODE_MARK_SECTION(node_half, tmp_section);
    MEM_SECTION_INC_COUNT(tmp_section);                           //使当前section的node数加两个
    MEM_SECTION_INC_COUNT(tmp_section);

    return mem_range_split_section(mem_range, mem_section);       //返回值这里递归调用该函数，直到找到一个合适的内存区间
}

unsigned long mem_node_alloc_pages(unsigned long count, unsigned long flags)  //从node中分配页，flags是标志哪个zone，count是标志几个页
{
    if (!count)
        return 0;
    if (count > MEM_SECTION_MAX_SIZE) {
        keprint(PRINT_NOTICE "%s: page count %d too big!\n", __func__, count);
        return 0;
    }
    mem_range_t *mem_range = NULL;

    if (flags & MEM_NODE_TYPE_DMA)
        mem_range = &mem_ranges[MEM_RANGE_DMA];
    else if (flags & MEM_NODE_TYPE_NORMAL)
        mem_range = &mem_ranges[MEM_RANGE_NORMAL];
    else if (flags & MEM_NODE_TYPE_USER)
        mem_range = &mem_ranges[MEM_RANGE_USER];
    else
        panic("phymem: get range null!");
    
    mem_section_t *mem_section;
    int i;
    for (i = 0; i < MEM_SECTION_MAX_NR; i++) {
        mem_section = &mem_range->sections[i];
        if (mem_section->section_size >= count) {                  //这里是从比count大一点的section开始分配，如果找到了就break，如果没有找到那么当前的section是最大order的section
            break;
        }
    }
    unsigned long intr_flags;
    interrupt_save_and_disable(intr_flags);
    if (list_empty(&mem_section->free_list_head)) {                //判断当前section的free_list_head是否为空
        if (mem_section->section_size == MEM_SECTION_MAX_SIZE) {    // 没有更大的节，意味着当前的buddy系统没有合适的空间了，需要进行碎片化整理
            // TODO: 收缩内存，合并没有使用的小节为大节
            mem_range_merge_section(mem_range);
            //keprint(PRINT_ERR "mempool: no free section!\n");
            interrupt_restore_state(flags);
            return 0;
        } else {
            if (mem_range_split_section(mem_range, mem_section) < 0) {
                keprint(PRINT_ERR "mempool: split section failed!\n");
                interrupt_restore_state(intr_flags);
                return 0;
            }
        }
    }
    mem_node_t *node = list_first_owner(&mem_section->free_list_head, mem_node_t, list);         //找到这个节点所归属的节的链表头
    list_del_init(&node->list);      //将该节中自由链表的第一个节点删除，因为已经分配了
    MEM_SECTION_DES_COUNT(mem_section);  //将该节中的node数-1

    mem_node_init(node, 1, count);       //将node分配count个页面并将node初始化
    MEM_NODE_MARK_SECTION(node, mem_section);  //标记该node所属的section
    interrupt_restore_state(intr_flags);
    return mem_node_to_phy_addr(node);    //返回这个node的物理地址
}

int mem_node_free_pages(unsigned long addr)         //将节点中的pages全部释放
{
    if (!addr)
        return -1;
    mem_node_t *node = phy_addr_to_mem_node(addr);
    if (!node)
        return -1;
    
    // 放回节点所属的节中去
    mem_section_t *section = MEM_NODE_GET_SECTION(node);
    if (!section) {
        // keprint(PRINT_WARING "node %x addr %x no section!\n", node, addr);
        return -1;
    }
    unsigned long intr_flags;
    interrupt_save_and_disable(intr_flags);
    if (list_find(&node->list, &section->free_list_head)) {          //从该节的可用链表中寻找这个节点，如果找到了返回-1，避免重复释放，没有找到继续下一步
        // keprint(PRINT_WARING "addr %x don't need free again!\n", addr);
        interrupt_restore_state(intr_flags); 
        return -1;
    }
    mem_node_init(node, 0, 0);  //这里的第三个参数是节点中页数
    list_add(&node->list, &section->free_list_head);    //把这个空了的节点放进节的可用链表中
    MEM_SECTION_INC_COUNT(section);                     //section的node+1
    interrupt_restore_state(intr_flags);
    return 0;
}

unsigned long mem_get_free_page_nr()                     //得到当前内存的未分配页的数目
{
    unsigned long flags;
    interrupt_save_and_disable(flags);                   //关中断
    size_t page_count = 0;
    int i, j;
    for (j = 0; j < MEM_RANGE_NR; j++) {
        mem_range_t *range = &mem_ranges[j];             //选择排序法，外层递归zone
        for (i = 0; i < MEM_SECTION_MAX_NR; i++) {
            mem_section_t *section = &range->sections[i];   //内层递归section
            page_count += section->node_count * section->section_size;
        }
    }
    interrupt_restore_state(flags);
    return page_count;
}
//内存碎片整理机制
/*
    函数输入：void
    函数输出：void
    函数功能：内存碎片整理
    实现细节：通过node结构体的ref参数判断该node是否被占用，如果被占用的话，
             判断该node的count参数，count是当前节点占用的page数，接下来找到该node所归属的section，判断section_size，
             这个参数表示该section所属的order的页数，即：在这个section中，每个节点应该拥有多少页，
             将当前node结构体的地址加上section_size，就能得到与该node相邻的node结构体，
             下面判断这个结构体是否是原node的伙伴：判断逻辑是看ref是否为1，为1则占用，下面判断该node所属的section的section_size是否与
             前面的node所属的section的section_size一样，如果一样，那么他俩属于伙伴，可以将其合并为更高一阶的section
*/

//建立buddy系统的合并机制
/*
    函数输入：range
    函数输出：void
    函数功能：在buddy系统内存不足时，启动本函数，本函数将低阶的section中的free_list_head合并为高阶的section
    实现细节：从order为0的section中的free_list_head链表开始判断，找到该链表的第一个节点，判断其ref是否为1，若为1，则其被占用，将其略过
             若为0，判断该node所属的section的section_size，并记录。将本node的node_base+section_size就得到其伙伴的node，判断伙伴node所属的section的section_size是否与其一致
             若一致，且ref为0，就可以将其合并为高一阶section中的节点，将其加入到高一阶section的free_list_head链表中，依次循环。
*/
void mem_range_merge_section(mem_range_t *mem_range){
    mem_section_t *mem_section;
    int i = 0;
    //mem_node_t *node_base = mem_range->node_table;  
    for(i = 0; i< MEM_SECTION_MAX_NR; i++){
        mem_section = &mem_range->sections[i];
        if(list_empty(&mem_section->free_list_head)){
            //如果第i阶的可用链表是空的，那么该阶不需要合并，直接执行i+1阶
            continue;
        }
        if(mem_section->section_size = MEM_SECTION_MAX_SIZE){
            //如果已经是最高阶了，就没有必要再继续了，直接返回
            return;
        }
        mem_node_t *node = list_first_owner(&mem_section->free_list_head, mem_node_t, list);  //找到这个空闲链表的第一个成员节点
        while(1){
            //这里用死循环遍历该order的free_list的所有成员，当遍历完成后，break
            unsigned int addr = mem_node_to_phy_addr(node);
            if(addr == 0){
                //这个函数返回0意味着没有找到该node的地址
                node = node->list.next;//node指向该节点链表的下一个节点
                continue;
            }
            //执行流走到这里，意味着找到了该node的地址，在bootmem初始化时，为每一个page都预留了一个node结构体，而node结构体中的
            //count参数表明该node含有多少个page，故node的地址+count*sizeof(mem_node_t)即是该node的伙伴
            addr += (node->count * SIZEOF_MEM_NODE);  //这时addr指向了该node的伙伴node的地址
            if(addr < mem_range->start || addr > mem_range->end){
                break;  //死循环结束
            }
            mem_node_t *tmp_node = phy_addr_to_mem_node(addr); //通过addr找到该伙伴node
            if(tmp_node->reference == 0 && tmp_node->section->section_size == node->section->section_size){
                //如果这个节点没有被占用，并且和原节点属于同一阶，将他们合并
                list_del(&node->list); 
                list_del(&tmp_node->list);  //将这两个节点从其所属section的free_list_head链表中删除
                MEM_SECTION_DES_COUNT(mem_section);
                MEM_SECTION_DES_COUNT(mem_section);//因为少了两个节点，所以当前section的node_count减少两个
                mem_node_init(node, 0, mem_section->section_size * 2);//将两个相邻的node合二为一，初始化为一个高一阶的node
                ++mem_section; // 增加一个节高度
                list_add(&node->list, &mem_section->free_list_head);  //将新node加入高一阶section的free_list_head中
                MEM_NODE_MARK_SECTION(node, mem_section);   //表示该node的所属section
                MEM_SECTION_INC_COUNT(mem_section);         //该section的node_count+1
            }
        }
    }
}


void mem_pool_test()
{
    uint32_t addr = 64 * MB;
    mem_node_t *node = phy_addr_to_mem_node(addr);
    keprint("addr mem node: %x\n", node);
    keprint("new addr: %x\n", mem_node_to_phy_addr(node));

    #if 1
    int i;
    for (i = 0; i < MEM_SECTION_MAX_NR; i++) {    
        addr = mem_node_alloc_pages(powi(2, i), MEM_NODE_TYPE_NORMAL);
        keprint("alloc addr: %x\n", addr);
        if (!addr)
            break;
        mem_node_free_pages(addr);
    }
    for (i = 0; i < MEM_SECTION_MAX_NR / 2; i++) {    
        addr = mem_node_alloc_pages(powi(2, i), MEM_NODE_TYPE_DMA);
        keprint("2 alloc addr: %x\n", addr);
        if (!addr)
            break;
        mem_node_free_pages(addr);
    }
    for (i = 0; i < MEM_SECTION_MAX_NR; i++) {    
        addr = mem_node_alloc_pages(powi(2, i), MEM_NODE_TYPE_USER);
        keprint("2 alloc addr: %x\n", addr);
        if (!addr)
            break;
        mem_node_free_pages(addr);
    }
    
    #endif
    spin("test");
}
