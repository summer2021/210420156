#include <arch/mempool.h>
#include <xbook/softirq.h>

static inline void add_page_to_lru_list(mem_node_t *mem_node,
				struct lruvec *lruvec, enum lru_list lru)
{
	list_add(&mem_node->list, &lruvec->lists[lru]);
}

void shrink_mem_ranges ();