#include "atomic.h"
#include <xbook/list.h>
/* 
    本文件定义了节点，区域和页的结构体，其中节点默认只有一个，区域有三个DMA、NORMAL、USER
    页用buddy系统进行分配

*/
#define GFP_ZONEMASK 0x03
#define MAX_ORDER 11
#define MAX_ZONELISTS 1  //最大zonelist数，当前zone空闲页面耗尽后，可以从其他zone获取页面
#define MAX_NUMNODES 1   //系统的最大节点数

enum {
    ZONE_DMA = 0,
    ZONE_NORMAL,
    ZONE_USER,
    MAX_NR_ZONES
};
enum migratetype{
    MIGRATE_UNMOVABLE,//就定义可移动和不可移动两种内存类型
    MIGRATE_MOVABLE,
    MIGRATE_TYPES
};


struct free_area {
    list_t free_list[MIGRATE_TYPES];
    unsigned long nr_free;//自由页的个数
};
struct page
{
    unsigned long flags;           //存储了体系结构无关的标志，用于描述页的属性
    atomic_t count;                //是一个使用计数，表示内核中应用该页的次数。在其值到达0时，内核就知道page实例当前不使用，因此可以删除；如果其值大于0，该实例绝不会从内存删除。
    list_t list;         /* 映射有一些页面列表*/
    //struct address_space *mapping; //本页所归属的节点
    unsigned long index;           //在映射内的偏移量
    list_t lru;          //是一个表头，用于在各种链表上维护该页，一遍将页按不用类别分组，最重要的类别是活动页和不活动页
 /*   
 union
    {
        struct pte_chain *chain; /* Reverse pte mapping pointer.
                * protected by PG_chainlock 
        pte_addr_t direct;
    } pte;这个是干啥的？
********////////
    //下面这个也不知道是干啥的，先不加进去了
    //unsigned long private; /* mapping-private opaque data */

    /*
    * On machines where all RAM is mapped into kernel address space,
    * we can simply calculate the virtual address. On machines with
    * highmem some memory is mapped into kernel virtual memory
    * dynamically, so we need a place to store that address.
    * Note that this field could be 16 bits on x86 ... ;)
    *
    * Architectures with slow multiplication can define
    * WANT_PAGE_VIRTUAL in asm/page.h
    */
#if defined(WANT_PAGE_VIRTUAL)
    void *virtual; /* Kernel virtual address (NULL if
                  not kmapped, ie. highmem) */
#endif             /* WANT_PAGE_VIRTUAL */
};

/*-----------------------------------zone--------------------------------------------------------------------------
该结构比较特殊的方面是它由ZONE_PADDING分割为几个部分。这是因为对zone结构的访问非常频繁。
在多处理器系统上，通常会有不同的CPU试图同时访问结构成员。因此使用锁（后面的博客会详细介绍）防止它们彼此干扰，避免错误和不一致。
由于内核对该结构的访问非常频繁，因此会经常性地获取该结构的两个自旋锁zone->lock和zone->lru_lock。
如果数据保存在CPU高速缓存中，那么会处理得更快速。高速缓存分为行，每一行负责不同的内存区。
内核使用ZONE_PADDING宏生成“填充”字段添加到结构中，以确保每个自旋锁都处于自身的缓存行中。
还使用了编译关键字__cacheline_internodealigned_in_smp，用以实现最优的高速缓存对齐方式。
该结构的最后两个部分也通过填充字段彼此分隔开来。两者都不包含锁，主要目的是将数据保持在一个缓存行中，便于快速访问，从而无需从内存加载数据。
由于填充字段造成结构长度的增加是可以忽略的，特别是在内核内存中zone结构的实例相对很少。
--------------------------------------------------------------------------------------------------------------------------*/
struct zone
{
    /*
    * Commonly accessed fields:
    */
    //spinlock_t    lock; //访问锁，保证同一时刻只有一个CPU访问该zone，本例只有一个CPU，用不到，注释掉
    unsigned long free_pages;                       //该区域自由页的数量
    unsigned long pages_min, pages_low, pages_high; //如果空闲页多于pages_high，则内存域的状态时理想的；如果空闲页的数目低于pages_low，则内核开始将页换出到硬盘；如果空闲页低于pages_min，那么页回收工作的压力就比较大，因为内核中急需空闲页。

    //ZONE_PADDING(_pad1_)  //这个宏是针对CPU进行优化，这里也用不到

    //spinlock_t    lru_lock;
    //这一部分涉及的结构成员，用来根据活动情况对内存域中使用的页进行编目，如果页访问频繁，则内核认为它是活动的；
    //而不活动的页则显然相反。在需要换出页时，这种区别是很重要的，如果可能的话，频繁使用的页应该保持不动，而多余的不活动的页则可以换出而没有什么影响。
    list_t active_list;   //活动页的集合,这里我用于表示已使用页
    list_t inactive_list; //不活动页的集合，表示未使用页
    //atomic_t refill_counter;    //不知道干嘛的，注释掉吧~~
    unsigned long nr_active;     //活动页的个数（已使用的总数）
    unsigned long nr_inactive;   //不活动页的个数（未使用页的总数）
    //int all_unreclaimable;       不知道干嘛的？
    //unsigned long pages_scanned; /*感觉用不到~~~ */

    //ZONE_PADDING(_pad2_)

    /*
    * prev_priority holds the scanning priority for this zone.  It is
    * defined as the scanning priority at which we achieved our reclaim
    * target at the previous try_to_free_pages() or balance_pgdat()
    * invokation.
    *
    * We use prev_priority as a measure of how much stress page reclaim is
    * under - it drives the swappiness decision: whether to unmap mapped
    * pages.
    *
    * temp_priority is used to remember the scanning priority at which
    * this zone was successfully refilled to free_pages == pages_high.
    *
    * Access to both these fields is quite racy even on uniprocessor.  But
    * it is expected to average out OK.
    */
    //int temp_priority;这两个权限的东西下次升级系统再加，现在不用
    //int prev_priority;

    /*
    * free areas of different sizes
    */
    struct free_area free_area[MAX_ORDER]; //用于实现伙伴系统，每个数组元素都表示某种固定长度的一些连续内存区，对于包含在每个区域中的空闲内存页的管理，free_area是一个起点。

    /*
    * wait_table     -- the array holding the hash table
    * wait_table_size -- the size of the hash table array
    * wait_table_bits -- wait_table_size == (1 << wait_table_bits)
    *
    * The purpose of all these is to keep track of the people
    * waiting for a page to become available and make them
    * runnable again when possible. The trouble is that this
    * consumes a lot of space, especially when so few things
    * wait on pages at a given time. So instead of using
    * per-page waitqueues, we use a waitqueue hash table.
    *
    * The bucket discipline is to sleep on the same queue when
    * colliding and wake all in that wait queue when removing.
    * When something wakes, it must check to be sure its page is
    * truly available, a la thundering herd. The cost of a
    * collision is great, but given the expected load of the
    * table, they should be so rare as to be outweighed by the
    * benefits from the saved space.
    *
    * __wait_on_page_locked() and unlock_page() in mm/filemap.c, are the
    * primary users of these fields, and in mm/page_alloc.c
    * free_area_init_core() performs the initialization of them.
    */
    //一下三个变量实现了一个等待队列，可用于等待某一页变为可用的进程，进程排成一个队列，等待某些条件，在条件变为真时，内核会通知进程恢复工作。
    //wait_queue_head_t  * wait_table;
    //unsigned long     wait_table_size;
    //unsigned long     wait_table_bits;
    /*
    以上部分都用不到，先注释掉
   */

    //ZONE_PADDING(_pad3_)

    //struct per_cpu_pageset pageset[NR_CPUS];

    /*
    * Discontig memory support fields.
    */
    struct pglist_data *zone_pgdat; //该区域和父节点的映射
    struct page *zone_mem_map;      //区域中页的映射集合
    /* zone_start_pfn == zone_start_paddr >> PAGE_SHIFT */
    unsigned long zone_start_pfn; //内存域第一个页帧的索引

    /*
    * rarely used fields:
    */
    char *name;                  //该区域的名字，现在有三种，DMA、NORMAL、USER
    unsigned long spanned_pages; //指定内存域中页的总数，但并非所有的都可用，因为有空洞
    unsigned long present_pages; //指定了内存域中实际上可用的页数目
} ____cacheline_maxaligned_in_smp;

/*
 * One allocation request operates on a zonelist. A zonelist
 * is a list of zones, the first one is the 'goal' of the
 * allocation, the other zones are fallback zones, in decreasing
 * priority.
 *
 * Right now a zonelist takes up less than a cacheline. We never
 * modify it apart from boot-up, and only a few indices are used,
 * so despite the zonelist table being relatively big, the cache
 * footprint of this construct is very small.
 */

//这个zonelist不知道怎么用
struct zonelist
{
    struct zone *zones[MAX_NUMNODES * MAX_NR_ZONES + 1]; // NULL delimited
};

typedef struct pglist_data
{
    struct zone node_zones[MAX_NR_ZONES];          //是一个数组，包含了结点中各内存域的数据结构
    //struct zonelist node_zonelists[MAX_ZONELISTS]; //指点了备用结点及其内存域的列表，以便在当前结点没有可用空间时，在备用结点分配内存
    int nr_zones;                                  //保存结点中不同内存域的数目

/****************************************************************************************************************
注释掉，这部分看不懂
#ifdef CONFIG_FLAT_NODE_MEM_MAP
    struct page *node_mem_map; //指向page实例数组的指针，用于描述结点的所有物理内存页，它包含了结点中所有内存域的页。
#endif
*****************************************************************************************************************/
//    struct bootmem_data *bdata; //在系统启动期间，内存管理子系统初始化之前，内核页需要使用内存（另外，还需要保留部分内存用于初始化内存管理子系统）。
//引导内存仍然使用xbook2的部分
/*这个也注释掉
#ifdef CONFIG_MEMORY_HOTPLUG
    spinlock_t node_size_lock;
#endif
*/
    unsigned long node_start_pfn;     //该NUMA结点第一个页帧的逻辑编号。系统中所有的页帧是依次编号的，每个页帧的号码都是全局唯一的（不只是结点内唯一）。
    //unsigned long node_present_pages; //结点中页帧的数目
    //感觉本项目不需要在节点中保存页帧个数
    //unsigned long node_spanned_pages; //该结点以页帧为单位计算的长度，包含内存空洞。
    int node_id;                      //全局结点ID，系统中的NUMA结点都从0开始编号
    //wait_queue_head_t kswapd_wait;    //交换守护进程的等待队列，在将页帧换出结点时会用到。
    //struct task_struct *kswapd;       //指向负责该结点的交换守护进程的task_struct。
    //int kswapd_max_order;             //定义需要释放的区域的长度。
} pg_data_t;
   




